/*ヘッダーハンバーガーメニュー*/
jQuery(function ($) {
  const mediaQueryList = window.matchMedia('(min-width: 1000px)');
  const listener = (event) => {
    // リサイズ時に行う処理
    if (event.matches) {
      // 999px以上
      $('#header').removeClass('active');
      $('#sp_nav').show();
      $('body').css('margin-top', 0);

      $('#header nav a:not(.nosmooth)[href*="#"]').click(function(){
        $('#sp_nav').show();
      });
    } else {
      // 999px未満
      const height = $('#header').height();
      $('#header').removeClass('active');
      $('#sp_nav').hide();
      $('body').css('margin-top', height);

      $('#header nav a:not(.nosmooth)[href*="#"]').click(function(){
        $('#header').removeClass('active');
        $('#sp_nav').hide();
      });
    }
  };
  mediaQueryList.addEventListener("change", listener);
  listener(mediaQueryList);
  
  $('#hamburger_btn').click(function() {
    $('#header').toggleClass('active');
    $('#sp_nav').slideToggle();
  });
});


/*よくある質問アコーディオン*/
jQuery(function ($) {
  $('#faq_accordion dl').removeClass('active');
  $('#faq_accordion dd').hide();
  $('#faq_accordion dt').click(function(){
    $(this).parent('dl').toggleClass('active');
    $(this).next('dd').slideToggle();
  });
});


/*スムーススクロール*/
jQuery(function ($) {
  $('a:not(.nosmooth)[href*="#"]').click(function(){
    const adjust= 50;
    const speed = 400;
    const target = $(this.hash === '#' || '' ? 'html' : this.hash)
    if(!target.length) return;
    const position = target.offset().top - adjust;
    $('body,html').animate({scrollTop:position}, speed, 'swing');
    return false;
  });
});

/*スクロールヒント*/
window.addEventListener('DOMContentLoaded', function(){
  new ScrollHint('.js-scrollable');
});