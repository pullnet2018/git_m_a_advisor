<?php get_header(); ?>

	<div id="contents">
		<!--メインコンテンツ-->
<div class="content-width clearfix clear BnrArea">
<div class="Inner">
<div class="Illust"><img src="https://mono-support.com/wp-content/themes/customize/img/top_obj.png" alt="代表：駒田裕次郎">
</div>
<div class="Text">
	<h1>事業再構築補助金・ものづくり申請代行サポート</h1>
<p class="Head">事業再構築補助金・ものづくり申請代行サポート（ＣＰＡ）とは、<br> 駒田総合会計事務所の運営する中小企業向けの補助金の申請をサポートするための情報サイトです。</p>
	　<span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">弊社サポートにより第1回･第2回の公募で6,000万円（通常枠）が採択されました！</span><a href="https://mono-support.com/saikouchiku/saitaku-2/"><u>採択結果はこちら</u></a>
<p><span>事業再構築補助金、ものづくり補助金を始め、各種補助金・助成金・事業計画のサポート実績、申請代行サポートは300件を超え</span>、そこでのノウハウを提供し、皆様のお役に立てればと思います。</p>



<span>事業再構築補助金、ものづくり補助金等を含む中小企業様向けの各種補助金・助成金の申請サポート、申請代行のご相談は、実績豊富な事業再構築補助金・ものづくり申請代行サポート(CPA）にお気軽にご相談ください。</span><br>第５回の公募（５次公募）が2022年1月20日に開始し、3月24日まで実施予定です。随時、無料相談を承っております。<br><br>
<a href="https://mono-support.com/lp-s2/"><span style="background: linear-gradient(transparent 70%, #fec1fe 0%);">事業再構築補助金の申請サポートはこちら</span></a><br>
<a href="https://mono-support.com/lp/"><span style="background: linear-gradient(transparent 70%, #bfff7f 0%);">ものづくり補助金の申請サポートはこちら</span></a><br><br>
<span class="color-button01-big"><a href="https://mono-support.com/about-us/contact/">お問い合わせはこちら</a></span><br>
</div>
</div>
</div>
		<?php if( is_toppage_style() == "one_column" ) : ?>
			<?php if( ! is_mobile()) :?>
			<main id="main-contents-one" class="main-contents <?php is_animation_style(); ?>" itemscope itemtype="https://schema.org/Blog">

				<?php if ( wp_isset_widets( 'home-top-widget',true ) ) : ?>
				<div id="home-top-widget">
				<?php dynamic_sidebar( 'home-top-widget' ); ?>
				</div>
				<?php endif; ?>

				<?php get_template_part('include/liststyle/post-list-mag'); ?>

				<?php if ( wp_isset_widets( 'home-bottom-widget',true ) ) : ?>
				<div id="home-bottom-widget">
				<?php dynamic_sidebar( 'home-bottom-widget' ); ?>
				</div>
				<?php endif; ?>

			</main>

			<?php else: ?>

			<main id="main-contents" class="main-contents <?php is_animation_style(); ?>" itemscope itemtype="https://schema.org/Blog">

				<?php if ( wp_isset_widets( 'home-top-widget',true ) ) : ?>
				<div id="home-top-widget">
				<?php dynamic_sidebar( 'home-top-widget' ); ?>
				</div>
				<?php endif; ?>
				<?php if( is_post_list_style() == "magazinestyle" ) : ?>
					<?php get_template_part('include/liststyle/post-list-mag'); ?>
				<?php elseif( is_post_list_style() == "magazinestyle-sp1col" ) : ?>
					<?php get_template_part('include/liststyle/post-list-mag-sp1col'); ?>
				<?php elseif( is_post_list_style() == "basicstyle" ) : ?>
					<?php get_template_part('include/liststyle/post-list'); ?>
				<?php endif; ?>

				<?php if ( wp_isset_widets( 'home-bottom-widget',true ) ) : ?>
				<div id="home-bottom-widget">
				<?php dynamic_sidebar( 'home-bottom-widget' ); ?>
				</div>
				<?php endif; ?>

			</main>
			<?php get_sidebar(); ?>

			<?php endif; ?>

		<?php elseif( is_toppage_style() == "two_column" ) : ?>

			<main id="main-contents" class="main-contents <?php is_animation_style(); ?>" itemscope itemtype="https://schema.org/Blog">

				<?php if ( wp_isset_widets( 'home-top-widget',true ) ) : ?>
				<div id="home-top-widget">
				<?php dynamic_sidebar( 'home-top-widget' ); ?>
				</div>
				<?php endif; ?>

				<?php if( is_post_list_style() == "magazinestyle" ) : ?>
					<?php get_template_part('include/liststyle/post-list-mag'); ?>
				<?php elseif( is_post_list_style() == "magazinestyle-sp1col" ) : ?>
					<?php get_template_part('include/liststyle/post-list-mag-sp1col'); ?>
				<?php elseif( is_post_list_style() == "basicstyle" ) : ?>
					<?php get_template_part('include/liststyle/post-list'); ?>
				<?php endif; ?>

				<?php if ( wp_isset_widets( 'home-bottom-widget',true ) ) : ?>
				<div id="home-bottom-widget">
				<?php dynamic_sidebar( 'home-bottom-widget' ); ?>

				</div>
				<?php endif; ?>

			</main>
			<?php get_sidebar(); ?>

		<?php endif; ?>

	</div>

<?php get_footer(); ?>
