<?php get_header(); ?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/style220218.css">

	<div id="contents">
		<!--メインコンテンツ-->
		<div class="content-width clearfix clear BnrArea">
			<div class="top_sec">
				<section class="sec01">
					<div class="w1140">
						<div class="col_wrap">
							<div>
								<h2>事業再構築補助金・<br>ものづくり申請代行<br>サポート</h2>
								<p>
									事業再構築補助金・ものづくり申請代行サポート（ＣＰＡ）とは、駒田総合会計事務所の運営する中小企業向けの補助金の申請をサポートするための情報サイトです。<br>
									事業再構築補助金、ものづくり補助金を始め、各種補助金・助成金・事業計画のサポート実績、申請代行サポートは300件を超え、そこでのノウハウを提供し、皆様のお役に立てればと思います。
								</p>
							</div>
							<div><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img01.png" alt="事業再構築補助金・ものづくり申請代行サポート"></div>
						</div>
						<p class="text_b">
							事業再構築補助金、ものづくり補助金等を含む中小企業様向けの各種補助金・助成金の申請サポート、申請代行のご相談は、実績豊富な事業再構築補助金・ものづくり申請代行サポート(CPA）にお気軽にご相談ください。
						</p>
					</div>
				</section>

				<section class="sec02">
					<div class="w1140">
						<div class="col_wrap">
							<div>
								<h2>事業再構築補助金とは</h2>
								<p>事業再構築補助金とは、新型コロナウィルスの影響を受け売上が減少している企業を対象に、事業再構築指針に基づき、新分野展開などの新規事業に取り組む場合の費用を補助する補助金になります。最大1億円かつ2/3の補助率という規模が大きい補助金です。<br>
									コロナ禍において事業の再構築や新規事業に取り組もうとしている企業にとっては、非常に魅力のある補助金といえます。<br>
									事業の再構築のために新分野展開や事業転換をご検討の企業様は、可能な限り申し込みの検討をしてみることをおすすめします。
								</p>
							</div>
							<div><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img02.jpg" alt="事業再構築補助金とは"></div>
						</div>
						<table>
							<tr>
								<th>対象企業</th>
								<th>補助金額</th>
								<th>補助率</th>
							</tr>
							<tr>
								<td>中小企業(通常枠)</td>
								<td>8,000万円以下</td>
								<td>2/3</td>
							</tr>
							<tr>
								<td>中小企業(卒業枠)</td>
								<td>1億円以下</td>
								<td>2/3（400社限定）</td>
							</tr>
							<tr>
								<td>中堅企業(通常枠)</td>
								<td>8,000万円以下</td>
								<td>1/2,1/3（5,000社弱と推定）</td>
							</tr>
							<tr>
								<td>中堅企業(グローバルV字回復枠)</td>
								<td>1億円以下</td>
								<td>1/2（100社限定）</td>
							</tr>
						</table>
					</div>
				</section>

				<section class="sec03">
					<div class="w1140">
						<h2>事業再構築補助金活用事例</h2>
						<div class="col_wrap">
							<div><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img03.png" alt="事業再構築補助金活用事例"></div>
							<div>
								<p>弊社サポートにより事業再構築補助金第1回･第2回の公募で6,000万円（通常枠）が<a href="https://mono-support.com/saikouchiku/saitaku-2/">採択された実績</a>もございます。<br>
									<br>
									▼第５回の公募（５次公募）が<br>
									<span class="text_bl">2022年1月20日に開始し、3月24日まで実施予定</span>です。<br>
									随時、無料相談を承っておりますので、お気軽にお問い合わせ下さい。
								</p>
							</div>
						</div>
						<a href="https://mono-support.com/about-us/contact/" class="bl_btn">お問い合わせはこちら</a>

						<table>
							<tr>
								<th>卸売業，小売業</th>
								<td>コロナ禍で安定した経営が難しく、新事業として抗体検査所を開設する。補助金を活用して営業所の内装工事、看板作成、宣伝、研修、イベント出店を行う。</td>
							</tr>
							<tr>
								<th>教育，学習支援業</th>
								<td>AI技術を導入し、「非接触型身体計測システム」と、筋力値を可視化する「トルク計測ダンパー」を開発し、医療機関、介護事業  者向けに販売を開始する。</td>
							</tr>
							<tr>
								<th>宿泊業，飲食サービス業</th>
								<td>ポストコロナの市場ニーズに応えるデリバリーに特化したゴーストレストランの開設し、食のニューノーマルに対応した業態転換。</td>
							</tr>
							<tr>
								<th>学術研究，専門・技術サービス業</th>
								<td>コールセンター業務から得た知見を活かし自動販売機機械トラブル時の対応をデジタル化システムの販売に着手し、新分野への展開。</td>
							</tr>
							<tr>
								<th>情報通信業</th>
								<td>医療機関を対象に電子カルテのサポート業務から得た知見を活かし、電子カルテの制作・販売に着手し、新分野への展開を図る。</td>
							</tr>
							<tr>
								<th>情報通信業</th>
								<td>新型コロナウイルスの影響により収益モデルが大きく棄損し、売上が大きく減少。４Ｇの低速回線上でも可能なリアルタイムＶＲ配信プラットフォームの開発・サービス化</td>
							</tr>
							<tr>
								<th>建設業</th>
								<td>海外市場拡大が困難となったため、国内の農山村地区の古民家を改修し、宿泊施設・シェアスペースとして貸し出す事業に進出し、新分野への展開を図る。</td>
							</tr>
							<tr>
								<th>不動産業，物品賃貸業</th>
								<td>初期コストを抑えて手軽に出店できる設備付きの店舗・事務所の建設をし、ウィズコロナの経営にシフト。</td>
							</tr>
							<tr>
								<th>生活関連サービス業，娯楽業</th>
								<td>ＳＤＧｓをベースとした事業。温泉源泉という古くから日本にある資源を有効活用により、スキンケアという分野において革新的なサービスを提供する。</td>
							</tr>
							<tr>
								<th>宿泊業，飲食サービス業</th>
								<td>ＩｏＴを駆使した「無人コンパクトホテル」の経営で培ったＩｏＴ技術と高いデザイン性を用い、コロナ渦でも安定需要があるレンタルスペース業界に進出する。</td>
							</tr>
						</table>
					</div>
				</section>

				<section class="sec04">
					<div class="w1140">
						<h2>事業再構築補助金スケジュール</h2>
						<p>第1回〜第4回の公募期間はすでに終了しております。</p>
						<p class="text_b">2022年1月20日から事業再構築補助金の<span>第5回公募が始まっております。3月24日に締切</span>予定です。</p>
						<p>ご検討の方は少しでも採択率を上げるためにも早めにご準備することをおすすめいたします。</p>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img04.jpg" alt="事業再構築補助金スケジュール">
					</div>
				</section>

				<section class="sec05">
					<div class="w1140">
						<h2>駒田総合会計事務所について</h2>
						<div class="wh_list">
							<div class="col_wrap">
								<div>
									<h3>採択率９０％以上（直近実績）1,000万円以上の大型案件が得意</h3>
									<p>
										累計５億円以上、採択率９０％以上の実績でフルサポートいたします。 当社は1,000万円以上の大型案件を得意としております！<br>
										事業再構築補助金が採択されるかどうかは、【説得力のある事業計画書】を作成できるかで決まります。ほんの少しの差で「事業再構築補助金の採択を受けられなかった･･･」という事にならないように、申請代行サポートの経験豊富な当社にぜひご相談ください。
									</p>
								</div>
								<div><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img05.jpg" alt="採択率９０％以上"></div>
							</div>
							<div class="rep">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img06.png" alt="代表">
								<p>申込要件を満たしているかどうかご不安な方、初めて補助金を申請されるので
									やり方がわからない方もお気軽にお問合せください。</p>
							</div>
						</div>
						<div class="wh_list">
							<div class="col_wrap">
								<div>
									<h3>成功報酬型のため安心。<br>全国どこでも申請代行をサポート！</h3>
									<p>
										成功報酬型なので、安心してご依頼ください。万が一、不採択の場合は<span>着手金（15万円）のほか一切費用はかかりません。</span><br>
										北海道・沖縄を含む、全国でのサポート実績があり、ZoomでのオンラインのWEBお打ち合わせも可能です。
									</p>
								</div>
								<div><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img07.jpg" alt="成功報酬型のため安心。"></div>
							</div>
						</div>
						<div class="wh_list">
							<div class="col_wrap">
								<div>
									<h3>採択後のモニタリングが重要！<br>受給完了まで徹底サポート！</h3>
									<p>
										せっかく申請に通過しても、その後の要件を満たさないと補助金はおりません。<br>
										採択後に最も重要なモニタリングのサポートを当事務所では支援いたします。<br>
										また、弊社は会計事務所のため、中小企業投資促進税制等を利用した一括償却のご相談など、 節税面のアドバイスも可能です（詳細は顧問税理士の先生にご相談ください）。
									</p>
								</div>
								<div><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img08.jpg" alt="採択後Cのモニタリングが重要！"></div>
							</div>
						</div>
					</div>
				</section>

				<section class="sec06">
					<div class="w1140">
						<h2>事業再構築補助金申請代行の料金</h2>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img09.png" alt="事業再構築補助金申請代行の料金">
							<div class="rep">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img06.png" alt="代表">
							<p>初回相談は無料です！<br>
								オンラインにてお打ち合わせを行いますので、全国どこでも対応可能です！
							</p>
						</div>
					</div>
				</section>

				<section class="sec07">
					<div class="w1140">
						<h2>事業再構築補助金申請代行の流れ</h2>

						<ul>
							<li>
								<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img10.png" alt="お問い合わせ・無料相談"></p>
								<h3>お問い合わせ・無料相談</h3>
								<p>事業再構築補助金の受給の可能性について【無料判定】をさせていただきます。まずはお気軽にご相談ください。</p>
							</li>
							<li>
								<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img11.png" alt="サポート申し込み"></p>
								<h3>サポート申し込み</h3>
								<p>サポート希望される場合は、お申込みをお願いいたします。別途申込書を用意しております。</p>
							</li>
							<li>
								<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img12.png" alt="申請書作成・提出"></p>
								<h3>申請書作成・提出</h3>
								<p>弊社と二人三脚になっていただき、事業計画を含む申請の準備を一緒に進めます。提出書類に漏れがないか等、確認いたします。</p>
							</li>
							<li>
								<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img13.png" alt="採択の決定"></p>
								<h3>採択の決定</h3>
								<p>補助金の申請後、約１か月後に採択が決定します。採択後、交付申請手続きに進んでいただきます。</p>
							</li>
							<li>
								<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img14.png" alt="成功報酬のお支払い"></p>
								<h3>成功報酬のお支払い</h3>
								<p>補助金の採択後、成功報酬として料金をご請求いたします。</p>
							</li>
							<li>
								<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img15.png" alt="事業の完了"></p>
								<h3>事業の完了</h3>
								<p>事業が完了し、実績報告をしますと補助金の入金が行われます。</p>
							</li>
						</ul>
					</div>
				</section>

				<section class="sec08">
					<div class="w1140">
						<h2>よくある質問</h2>
						<p>事業再構築補助金に関するよくある質問をまとめております。ぜひご覧ください。</p>
							<div class="link_wrap">
							<a href="https://mono-support.com/saikouchiku/qa-saihen/" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/top_img16.jpg');">
								<h3><span>業態転換、事業再編</span><br>について</h3>
							</a>
							<a href="https://mono-support.com/saikouchiku/qa-shishin1/" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/top_img17.jpg');">
								<h3><span>事業再構築指針全般</span><br>について</h3>
							</a>
							<a href="https://mono-support.com/saikouchiku/qa-tenkan/" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/top_img18.jpg');">
								<h3><span>新分野展開、事業転換、<br>業種転換</span>について</h3>
							</a>
							<a href="https://mono-support.com/saikouchiku/qa-application-2/" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/top_img19.jpg');">
								<h3><span>申請要件、申請手続き</span><br>について</h3>
							</a>
						</div>

						<a href="https://mono-support.com/lp-s2/#question" class="bl_btn">その他ご質問はこちら</a>
					</div>
				</section>

				<section class="sec09">
					<div class="w1140">
						<h2>補助金に関する最新情報を随時発信</h2>
					</div>
				</section>
			</div>
		</div>

		<div style="overflow: hidden; max-width: 1200px; margin: 0 auto;">
			<?php if( is_toppage_style() == "one_column" ) : ?>
				<?php if( ! is_mobile()) :?>
				<main id="main-contents-one" class="main-contents <?php is_animation_style(); ?>" itemscope itemtype="https://schema.org/Blog">

					<?php if ( wp_isset_widets( 'home-top-widget',true ) ) : ?>
					<div id="home-top-widget">
					<?php dynamic_sidebar( 'home-top-widget' ); ?>
					</div>
					<?php endif; ?>

					<?php get_template_part('include/liststyle/post-list-mag'); ?>

					<?php if ( wp_isset_widets( 'home-bottom-widget',true ) ) : ?>
					<div id="home-bottom-widget">
					<?php dynamic_sidebar( 'home-bottom-widget' ); ?>
					</div>
					<?php endif; ?>

				</main>

				<?php else: ?>

				<main id="main-contents" class="main-contents <?php is_animation_style(); ?>" itemscope itemtype="https://schema.org/Blog">

					<?php if ( wp_isset_widets( 'home-top-widget',true ) ) : ?>
					<div id="home-top-widget">
					<?php dynamic_sidebar( 'home-top-widget' ); ?>
					</div>
					<?php endif; ?>
					<?php if( is_post_list_style() == "magazinestyle" ) : ?>
						<?php get_template_part('include/liststyle/post-list-mag'); ?>
					<?php elseif( is_post_list_style() == "magazinestyle-sp1col" ) : ?>
						<?php get_template_part('include/liststyle/post-list-mag-sp1col'); ?>
					<?php elseif( is_post_list_style() == "basicstyle" ) : ?>
						<?php get_template_part('include/liststyle/post-list'); ?>
					<?php endif; ?>

					<?php if ( wp_isset_widets( 'home-bottom-widget',true ) ) : ?>
					<div id="home-bottom-widget">
					<?php dynamic_sidebar( 'home-bottom-widget' ); ?>
					</div>
					<?php endif; ?>

				</main>
				<?php get_sidebar(); ?>

				<?php endif; ?>

			<?php elseif( is_toppage_style() == "two_column" ) : ?>

				<main id="main-contents" class="main-contents <?php is_animation_style(); ?>" itemscope itemtype="https://schema.org/Blog">

					<?php if ( wp_isset_widets( 'home-top-widget',true ) ) : ?>
					<div id="home-top-widget">
					<?php dynamic_sidebar( 'home-top-widget' ); ?>
					</div>
					<?php endif; ?>

					<?php if( is_post_list_style() == "magazinestyle" ) : ?>
						<?php get_template_part('include/liststyle/post-list-mag'); ?>
					<?php elseif( is_post_list_style() == "magazinestyle-sp1col" ) : ?>
						<?php get_template_part('include/liststyle/post-list-mag-sp1col'); ?>
					<?php elseif( is_post_list_style() == "basicstyle" ) : ?>
						<?php get_template_part('include/liststyle/post-list'); ?>
					<?php endif; ?>

					<?php if ( wp_isset_widets( 'home-bottom-widget',true ) ) : ?>
					<div id="home-bottom-widget">
					<?php dynamic_sidebar( 'home-bottom-widget' ); ?>

					</div>
					<?php endif; ?>

				</main>
				<?php get_sidebar(); ?>

			<?php endif; ?>
		</div>
	</div>

<?php get_footer(); ?>
