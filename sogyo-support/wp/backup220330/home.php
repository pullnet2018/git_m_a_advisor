<?php get_header(); ?>

	<div id="contents">

		<!--メインコンテンツ-->
<div class="content-width clearfix clear BnrArea">
<div class="Inner">
<div class="Illust"><img src="https://www.sogyo-support.biz/wp-content/themes/customize/img/top_obj.png" alt="">
</div>
<div class="Text">
	<h1>【公庫の創業融資】のサポートは創業融資 代行サポート（CPA）へ</h1>
	<p class="Head">創業融資 代行サポート（ＣＰＡ）とは、<span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">創業時の資金調達をサポートするための情報サイト</span>です。
	<p><span>日本政策金融公庫の創業融資の代行サポート実績は1,000件を超え</span>、そこでのノウハウを提供し、<br>
      皆様のお役に立てればと思います。</p>
	<span>創業時の<a href="https://www.jfc.go.jp/">公庫</a>での資金調達や資金繰りは、実績豊富な創業融資 代行サポート（CPA）にお気軽にご相談ください。</span>随時、代行の無料相談を承っております（運営：駒田総合会計事務所）。<br><a href="https://www.sogyo-support.biz/lp/"><span style="background: linear-gradient(transparent 70%, #fec1fe 0%);">創業融資の代行サポートはこちら</span></a></p>

<span class="color-button01-big"><a href="https://www.sogyo-support.biz/%E4%BA%8B%E5%8B%99%E6%89%80%E7%B4%B9%E4%BB%8B/%E3%81%8A%E5%95%8F%E3%81%84%E5%90%88%E3%82%8F%E3%81%9B%E3%83%95%E3%82%A9%E3%83%BC%E3%83%A0/">お問い合わせはこちら</a></span><br>
</div>
</div>
</div>	
		<?php if( is_toppage_style() == "one_column" ) : ?>
			<?php if( ! is_mobile()) :?>
			<main id="main-contents-one" class="main-contents <?php is_animation_style(); ?>" itemscope itemtype="https://schema.org/Blog">
					
				<?php if ( wp_isset_widets( 'home-top-widget',true ) ) : ?>
				<div id="home-top-widget">
				<?php dynamic_sidebar( 'home-top-widget' ); ?>
				</div>
				<?php endif; ?>

				<?php get_template_part('include/liststyle/post-list-mag'); ?>

				<?php if ( wp_isset_widets( 'home-bottom-widget',true ) ) : ?>
				<div id="home-bottom-widget">
				<?php dynamic_sidebar( 'home-bottom-widget' ); ?>
				</div>
				<?php endif; ?>
				
			</main>
		
			<?php else: ?>
		
			<main id="main-contents" class="main-contents <?php is_animation_style(); ?>" itemscope itemtype="https://schema.org/Blog">
				
				<?php if ( wp_isset_widets( 'home-top-widget',true ) ) : ?>
				<div id="home-top-widget">
				<?php dynamic_sidebar( 'home-top-widget' ); ?>
				</div>
				<?php endif; ?>

				<?php if( is_post_list_style() == "magazinestyle" ) : ?>
					<?php get_template_part('include/liststyle/post-list-mag'); ?>
				<?php elseif( is_post_list_style() == "magazinestyle-sp1col" ) : ?>
					<?php get_template_part('include/liststyle/post-list-mag-sp1col'); ?>
				<?php elseif( is_post_list_style() == "basicstyle" ) : ?>
					<?php get_template_part('include/liststyle/post-list'); ?>
				<?php endif; ?>
				
				<?php if ( wp_isset_widets( 'home-bottom-widget',true ) ) : ?>
				<div id="home-bottom-widget">
				<?php dynamic_sidebar( 'home-bottom-widget' ); ?>
				</div>
				<?php endif; ?>

			</main>
			<?php get_sidebar(); ?>
		
			<?php endif; ?>

		<?php elseif( is_toppage_style() == "two_column" ) : ?>
		
			<main id="main-contents" class="main-contents <?php is_animation_style(); ?>" itemscope itemtype="https://schema.org/Blog">
				
				<?php if ( wp_isset_widets( 'home-top-widget',true ) ) : ?>
				<div id="home-top-widget">
				<?php dynamic_sidebar( 'home-top-widget' ); ?>
				</div>
				<?php endif; ?>

				<?php if( is_post_list_style() == "magazinestyle" ) : ?>
					<?php get_template_part('include/liststyle/post-list-mag'); ?>
				<?php elseif( is_post_list_style() == "magazinestyle-sp1col" ) : ?>
					<?php get_template_part('include/liststyle/post-list-mag-sp1col'); ?>
				<?php elseif( is_post_list_style() == "basicstyle" ) : ?>
					<?php get_template_part('include/liststyle/post-list'); ?>
				<?php endif; ?>
				
				<?php if ( wp_isset_widets( 'home-bottom-widget',true ) ) : ?>
				<div id="home-bottom-widget">
				<?php dynamic_sidebar( 'home-bottom-widget' ); ?>
				</div>
				<?php endif; ?>

			</main>
			<?php get_sidebar(); ?>
			
		<?php endif; ?>
		
	</div>
	
<?php get_footer(); ?>
