<?php get_header(); ?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/style220330.css">

	<div id="contents">

		<!--メインコンテンツ-->
		<div class="content-width clearfix clear BnrArea">
		<div class="top_sec">
			<section class="sec01">
				<div class="w1140">
					<div class="col_wrap">
						<div>
							<h2>創業融資・公庫融資の<br>代行サポート</h2>
							<p>
								創業融資 代行サポート(CPA)とは、駒田総合会計事務所の運営するこれから創業される方・創業5年以内の方向けた創業時の資金調達をサポートするための情報サイトです。<br>
								創業融資とは、創業の前後に金融機関から融資を受けることです。<br>
								創業融資代行サポート（CPA）では、日本政策金融公庫の創業融資について万全の体制で代行サポートしております。
							</p>
						</div>
						<div><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img01.png" alt="創業融資・公庫融資の代行サポート"></div>
					</div>
					<p class="text_b">
						日本政策金融公庫の創業融資の代行サポート実績は1,000件を超え、そこでのノウハウを提供し、皆様のお役に立てればと思います。創業時の資金繰りについてはぜひお気軽にご相談下さい。
					</p>
					<div class="contact_wrap">
						<p><span>公庫</span>の<span>創業融資</span>に関する<br class="sp_only">お悩みを解決いたします</p>
						<div class="soudan">
							<p>毎月公庫での面談同行<br class="sp_only">サポートを実施中</p>
							<p>
								<span>相談無料！</span><br class="sp_only">お気軽にご相談ください
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img02.png">
							</p>
						</div>
						<div class="contact">
							<a href="tel:0120-005-015">
								<p>お電話でのお問い合わせはこちら</p>
								<p class="link_name tel"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/tel.png">0120-005-015</p>
								<p>平日10：00～18：00</p>
							</a>
							<a href="https://www.sogyo-support.biz/%E4%BA%8B%E5%8B%99%E6%89%80%E7%B4%B9%E4%BB%8B/%E3%81%8A%E5%95%8F%E3%81%84%E5%90%88%E3%82%8F%E3%81%9B%E3%83%95%E3%82%A9%E3%83%BC%E3%83%A0/">
								<p>メールでのお問い合わせはこちら</p>
								<p class="link_name"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/mail.png">お問い合わせフォーム</p>
								<p>24時間受付中</p>
							</a>
							<a href="https://line.me/R/ti/p/%40565ywvmq" target="_blank">
								<p>LINEでのお問い合わせはこちら</p>
								<p class="link_name"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/line.png">LINEでのご相談</p>
								<p>24時間受付中</p>
							</a>
						</div>
					</div>
				</div>
			</section>
			
			<section class="sec02">
				<div class="w1140">
					<h2>創業融資・公庫融資についてこんなお悩みありませんか？</h2>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img03.png" alt="創業融資・公庫融資についてこんなお悩みありませんか？" class="pc_only">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img03_sp.png" alt="創業融資・公庫融資についてこんなお悩みありませんか？" class="sp_only">
				</div>
			</section>
			
			<section class="sec03">
				<div class="w1140">
					<h2>創業融資は日本政策金融公庫がおすすめ！</h2>
					<p>
						日本政策金融公庫には、様々な創業者向けの融資制度があります。<br>
						特に以下は小さな起業家の方々にとっても利用しやすい制度と言われています。<br>
					</p>
					<div class="link_wrap">
						<a href="https://www.sogyo-support.biz/blog/sougyo/shinkikaigyoushikin/" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/top_img04.jpg');">
							<span>新規開業資金</span>
						</a>
						<a href="https://www.sogyo-support.biz/blog/sougyo/koukosougyouyushi/" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/top_img05.jpg');">
							<span>女性・若者・シニア<br>起業家資金</span>
						</a>
						<a href="https://www.sogyo-support.biz/blog/sougyo/shinsa1/" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/top_img06.jpg');">
							<span>新創業融資制度</span>
						</a>
					</div>
					<div class="rep">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img07.png" alt="代表">
						<p>公庫の創業融資では「無担保・無保証」の制度を利用できるため、民間の金融機関による創業融資（制度融資を始めとする保証協会付き融資等）などど比べますと、この点で非常に有利な方法と言えるでしょう。</p>
					</div>
					
					<h3 class="blue_ttl">日本政策金融公庫の審査について</h3>
					<p>公庫の審査は一発勝負です。一度審査に落ちてしまいますと、しばらく間は再チャレンジができません。また、面談対策も非常に重要になります。日本政策金融公庫の創業融資の審査に落ちないために、以下のポイントを抑えておくことをおすすめいたします。</p>
								
					<div class="check_wrap">
						<div class="check">
							<div>
								<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/check01.png" alt="check01"></p>
								<h4>斯業経験が十分かどうか</h4>
								<p>斯業経験とは起業しようと考えている事業と関連する仕事の経験のことを意味します。斯業経験を積んでいる事業者の方が起業リスクは低いと考えられます。<br>
									日本政策金融公庫総合研究所の「開業者の斯業経験と開業直後の業績」で斯業経験の有無による目標月商達成率には違いがあります。斯業経験のない方は少なくても2年以上の経験を積んでから創業融資を申し込みするようにしましょう。</p>
							</div>
							<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img08.png" alt="斯業経験が十分かどうか"></p>
						</div>
						<div class="check">
							<div>
								<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/check02.png" alt="check02"></p>
								<h4>自己資金は貯まっているか</h4>
								<p>自己資金が少ない状態で創業融資を申し込むと、計画性のない企業とみなされて審査にマイナス評価になります。自己資金の目安としては開業資金の2~3割程度がおすすめです。</p>
							</div>
							<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img09.png" alt="自己資金は貯まっているか"></p>
						</div>
						<div class="check">
							<div>
								<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/check03.png" alt="check03"></p>
								<h4>信用情報に傷ついていないか</h4>
								<p>信用情報とは公共料金、税金の支払いやローンの返済などお金の支払いがしっかりなされているかという情報のことをいいます。信用情報はCIC(信用情報機関)によって、金融機関を中心とする会員で共有しています。ローンやクレジットカードの支払いが数回遅れている場合、信用できる人物ではないとみなされ、融資を受けることができなくなります。</p>
							</div>
							<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img10.png" alt="信用情報に傷ついていないか"></p>
						</div>
						<div class="check">
							<div>
								<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/check04.png" alt="check04"></p>
								<h4>必要書類がしっかり書かれているか</h4>
								<p>必要書類が細かく、正確に書かれているかという点も重要です。創業融資は事業の実績がないため、審査の際、参考にすべきものは書類と面談しかありません。特に創業計画書は重要種類の一つなので、作成が難しい場合は会計事務所に相談することをお勧めいたします。</p>
							</div>
							<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img11.png" alt="必要書類がしっかり書かれているか"></p>
						</div>
					</div>
					
					<h3 class="blue_ttl">日本政策金融公庫関連の記事のご紹介</h3>
					<p>日本政策金融公庫での創業融資をご検討中の方は是非一度、下記の記事を一読しておくことをおすすめいたします。</p>
					<ul class="article_wrap">
						<li><a href="https://www.sogyo-support.biz/blog/sougyo/%e3%80%90%e4%bf%9d%e5%ad%98%e7%89%88%e3%80%91%e6%97%a5%e6%9c%ac%e6%94%bf%e7%ad%96%e9%87%91%e8%9e%8d%e5%85%ac%e5%ba%ab%e3%81%ae%e5%89%b5%e6%a5%ad%e8%9e%8d%e8%b3%87%e3%81%ab%e3%81%a4%e3%81%84%e3%81%a6/">日本政策金融公庫の創業融資について</a></li>
						<li><a href="https://www.sogyo-support.biz/blog/sougyo/recommendstartuploanprogram/">日本政策金融公庫のメリット・デメリット｜創業融資におすすめです</a></li>
						<li><a href="https://www.sogyo-support.biz/blog/sougyo/noownfundok/">日本政策金融公庫の新創業融資制度は自己資金なしでも利用できる？</a></li>
						<li><a href="https://www.sogyo-support.biz/blog/sougyo/necessarilydocument/">日本政策金融公庫の創設融資を受けるのにそろえるべき必要書類</a></li>
						<li><a href="https://www.sogyo-support.biz/blog/sougyo/businesspurpose/">日本政策金融公庫とは｜事業目的や融資制度を解説します</a></li>
						<li><a href="https://www.sogyo-support.biz/blog/sougyo/%e5%89%b5%e6%a5%ad%e8%9e%8d%e8%b3%87%e6%97%a5%e6%9c%ac%e6%94%bf%e7%ad%96%e9%87%91%e8%9e%8d%e5%85%ac%e5%ba%ab%e3%81%aa%e3%81%a9%e3%82%92%e6%9c%89%e5%88%a9%e3%81%ab%e9%80%b2%e3%82%81%e3%82%8b%e6%96%b9/">創業融資(日本政策金融公庫など)を有利に進める方法とは？【認定支援機関について】 </a></li>
						<li><a href="https://www.sogyo-support.biz/blog/sougyo/fascination/">日本政策金融公庫の金利|他金融機関より利用しやすい点が魅力</a></li>
						<li><a href="https://www.sogyo-support.biz/blog/sougyo/shinsa1/">日本政策金融公庫の創業融資とは？審査をする前に確認すべき4つのポイント </a></li>
						<li><a href="https://www.sogyo-support.biz/blog/sougyo-plan1/">創業融資の創業計画書の書き方！【日本政策金融金庫】</a></li>
						<li><a href="https://www.sogyo-support.biz/blog/sougyo/whichonedoyouchoose/">日本政策金融公庫の創業融資と制度融資の違い｜どちらを選ぶべき？</a></li>
						<li><a href="https://www.sogyo-support.biz/blog/sougyo/kouko-ochiru/">日本政策金融公庫の創業融資の審査で落ちる理由と対策方法</a></li>
						<li><a href="https://www.sogyo-support.biz/blog/sougyo/difficulty/">日本政策金融公庫の審査難易度は？通過のポイントもチェック</a></li>
						<li><a href="https://www.sogyo-support.biz/blog/sougyo/syoukoucyukin/">「商工中金」と「日本政策金融公庫」を比較！</a></li>
						<li><a href="https://www.sogyo-support.biz/blog/sougyo/shokochukin/">政府系金融機関である「商工中金」とは？日本政策金融金庫と比較！</a></li>
					</ul>
				</div>
			</section>

			<section class="sec04">
				<div class="w1140">
					<h2>創業融資・公庫融資の成功実績をご紹介</h2>

					<div class="achievement">
						<div class="ach_head">
							<div>
								<p class="case"><span>CASE<span>01</span></span></p>
							</div>
							<div>
							<p>創業融資</p>
								<h3>WEBマーケティング会社 自己資金200万円、融資額600万円</h3>
							</div>
						</div>
						<div class="ach_bottom">
							<div class="user">
								<div>
									<p class="ttl">創業計画書の書き方も全くわからないまま<br class="pc_only">
										スタッフの方のサポートで融資をいただけました。</p>
									<p>WEBマーケティング会社<br>
										代表取締役社長　Y.K様</p>
								</div>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img12.png" alt="WEBマーケティング会社 代表取締役社長　Y.K様">
							</div>
							<div class="comment">
								<div>
									<p>
										起業を始めたいと思ってましたが、最初に初期費用が多くかかる事業のため、手持ちの資金では全く足りませんでした。<br>
										そこで、創業融資を考えましたが、これまでに金融機関で融資を受けたこともなく、事業計画も作ったことがなかったので困り果てていたところ、創業融資 代行サポート（ＣＰＡ）を知りました。<br>
										<br>
										「果たして自分でも融資を受けることができるのか？」･･･最も気になっていた事ですが、<span>無料相談に予約して相談にのってもらったところ、十分に融資の可能性があることがわかりました。</span>また、事業計画のいろはもわからない中、作成をしっかりサポートしていただいたお陰で、 しっかりと自分の頭の中で整理ができました。<br>
										<br>
									</p>
								</div>
								<div>
									<p>
										その後、創業融資を申し込んだ金融機関（日本政策金融公庫）で審査面談があるとのことでしたが、 銀行での面談は初めてであり、非常に緊張していました。この点、 面談前にはリハーサルをしていただき、 心理的にもしっかりと準備して臨むことができました。<br><br>
									</p>
									<p class="point">
										・公庫や銀行で融資相談をしたことがない（銀行が怖い！）<br>
										<span>・事業計画を作ったことがない。</span><br>
										<span>・必ず創業融資を受けて、必要な資金を調達したい。</span><br>
									</p>
									<p>
										<br>私のようにこのように思っている方は、 一度ぜひ無料相談を受けてみてはいかがでしょうか？
									</p>
								</div>
							</div>
						</div>
					</div>

					<div class="achievement">
						<div class="ach_head">
							<div>
								<p class="case"><span>CASE<span>02</span></span></p>
							</div>
							<div>
								<p>創業融資</p>
								<h3>飲食店 自己資金300万円、融資額1,000万円</h3>
							</div>
					</div>
						<div class="ach_bottom">
							<div class="user">
								<div>
									<p class="ttl">日本政策金融公庫の創業融資のサポートを<br class="pc_only">
										お願いし、無事に資金調達ができました。</p>
									<p>飲食店経営<br>
										代表取締役社長　K.A様</p>
								</div>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img13.png" alt="飲食店経営 代表取締役社長　K.A様">
							</div>
							<div class="comment">
								<div>
									<p>
										ずっと飲食店を開業したいと思っていたところ、ちょうど良い物件が見つかりました。公庫で 創業計画書をもらいましたが、書き方が今イチわからず、途方に暮れていたところ、CPAの サイトを見つけ、無料相談に申し込んでみました。<br>
										<br>
										無料相談でわかったことは、<br>
									</p>
									<p class="point">
										・融資は一発勝負であり、やり直しがきかないこと。<br>
										・<span>物件を確保しつつ、融資の申込みを行うことが十分に可能</span>であること。<br>
										・<span>自己資金が少なめ</span>でも、経験をアピールできればうまく<span>乗り切れる可能性がある</span>こと。<br>
										・<span>金利０％台で、最短２～３週間</span>での融資が可能であること。<br>
									</p>
								</div>
								<div>
									<p>
										また、融資は書類審査のほか、面接も非常に大切だということがわかりました。<br>
										<br>
										ＣＰＡさんでは、公庫での面談対策もしっかりとやっていただけ、 お蔭様で無事に面接を乗り切ることができました。 面談同行サポートも非常に心強かったです。 <span>日本政策公庫の創業融資を受けた後、信用金庫で保証協会付き融資も 同時に受けることができた</span>ので、十分な資金を確保して お店を開業することができました。<br>
										<br>
										ＣＰＡさんは<span>成功報酬型</span>でしたので、 自己資金の少ない私には大変助かりました。<br>
										今後は経理や確定申告もお願いしようと思ってます。 <br>
									</p>
								</div>
							</div>
						</div>
					</div>

					<div class="achievement_others">
						<h3 class="blue_ttl">その他様々な業種にて創業融資・公庫融資の成功実績がございます</h3>
						<p>通常、公庫の創業融資が通る確率は<span>１～２割</span>とも言われていますが、当事務所のサポートでは<span>９９％以上</span>のお客様が公庫の創業融資を成功させ、新しいビジネスをスタートさせています。</p>
						
						<p class="table_ttl">創業融資　実績</p>
						<div class="table_wrap">
							<table>
								<tr>
									<th>業種</th>
									<th>融資金額</th>
									<th>融資タイプ</th>
								</tr>
								<tr>
									<td>飲食業(東京都)</td>
									<td>1,000万円</td>
									<td>日本政策金融公庫</td>
								</tr>
								<tr>
									<td>学習塾(千葉県)</td>
									<td>800万円</td>
									<td>日本政策金融公庫</td>
								</tr>
								<tr>
									<td>物販サービス業(大阪府)</td>
									<td>900万円</td>
									<td>日本政策金融公庫</td>
								</tr>
								<tr>
									<td>ネット小売業(福岡県)</td>
									<td>600万円</td>
									<td>保証協会付き融資</td>
								</tr>
								<tr>
									<td>コインランドリー業(神奈川県)</td>
									<td>1,500万円</td>
									<td>日本政策金融公庫</td>
								</tr>
							</table>
							
							<table>
								<tr>
									<th>業種</th>
									<th>融資金額</th>
									<th>融資タイプ</th>
								</tr>
								<tr>
									<td>スポーツジム(東京都)</td>
									<td>1,700万円</td>
									<td>日本政策金融公庫</td>
								</tr>
								<tr>
									<td>飲食業(東京都)</td>
									<td>1,500万円</td>
									<td>日本政策金融公庫</td>
								</tr>
								<tr>
									<td>整骨院(神奈川県)</td>
									<td>600万円</td>
									<td>日本政策金融公庫</td>
								</tr>
								<tr>
									<td>建設業(東京都)</td>
									<td>800万円</td>
									<td>日本政策金融公庫</td>
								</tr>
								<tr>
									<td>クラウド事業</td>
									<td>1,000万円</td>
									<td>保証協会付き融資</td>
								</tr>
							</table>
						</div>
						<p>その他多数</p>
					</div>
				</div>
			</section>
			
			<section class="sec05">
				<div class="w1140">
					<h2>創業融資・公庫融資 代行サポート(CPA)の強み</h2>
					
					<div class="strength">
						<h3>日本政策金融公庫の創業融資に強い！</h3>
						<div>
							<p>
								<span>公庫の創業融資</span>のサポート・代行は、当社にぜひお任せください！<br>
								<span>通過率９９％以上の実績</span>でフルサポートいたします。<br>
								日本政策金融公庫の創業融資を確実に受けるためには、 <font color="#0000ff">創業計画書の中身</font>だけでなく、<font color="#0000ff">公庫の面接での受け答えや自己資金の要件等</font>が非常に重要です。<br>
								ほんの少しの差で、「公庫の創業融資が受けられなかった･･･」、という事にならないように、創業融資の経験豊富な当社にぜひご相談ください。<br>
								創業融資は、<span>「無担保・無保証」</span>＆<span>「金利０.５％～」</span>で<span>「最短２～３週間」</span>に受けることもできますので、お気軽にご相談ください。<br>
								<span>公庫に加えて信用金庫の創業融資もダブルで受けたい方もお気軽にお問合せください。</span><br>
							</p>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img14.png" alt="日本政策金融公庫の創業融資に強い！">
						</div>
					</div>

					<div class="strength">
						<h3>完全成功報酬で安心、最短3週間のスピード対応！</h3>
						<div>
							<p>
								<span>完全成功報酬</span>ですので、安心してご相談ください。万が一、融資が通らなかった場合には、成功報酬は発生しません。<br>
								<br><font color="#0000ff">期間限定で先着5名様限定で成功報酬3％</font>で承ります。<br>
								公庫での面談がご不安の方には、<span>面談時の同席サポート</span>もしております。<br>
								<br>
								また、北海道・沖縄を含む、<span>全国でのサポート実績</span>があります。<br>
							</p>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img15.png" alt="成功報酬中心で安心。最短3週間のスピード対応！">
						</div>
					</div>

					<div class="strength">
						<h3>会社設立や税務会計もまとめてワンストップで提供いたします！</h3>
						<div>
							<p>
								<span>会社設立</span>や<span>税理士顧問</span>、許認可の申請など、ご要望に応じて創業時に必要なサービスをまとめて対応させていただきます。<br>
								創業を成功させるためために、経営者の方には事業に専念していただき、より多くの時間を本業のため使っていただきたいと考えています。<br>
								<br>
								弊社は<span>税理士・会計事務所であり</span>、その他にも司法書士・社会保険労務士・弁護士・行政書士など、経験豊富なパートナーがおりますので、ワンストップで対応できます。<br>
							</p>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img16.png" alt="会社設立や税務会計もまとめてワンストップで提供いたします！">
						</div>
					</div>
				</div>
			</section>

			<section class="sec06">
				<div class="w1140">
					<div class="contact_wrap">
						<p><span>公庫</span>の<span>創業融資</span>に関する<br class="sp_only">お悩みを解決いたします</p>
						<div class="soudan">
							<p>毎月公庫での面談同行<br class="sp_only">サポートを実施中</p>
							<p>
								<span>相談無料！</span><br class="sp_only">お気軽にご相談ください
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img02.png">
							</p>
						</div>
						<div class="contact">
							<a href="tel:0120-005-015">
								<p>お電話でのお問い合わせはこちら</p>
								<p class="link_name tel"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/tel.png">0120-005-015</p>
								<p>平日10：00～18：00</p>
							</a>
							<a href="https://www.sogyo-support.biz/%E4%BA%8B%E5%8B%99%E6%89%80%E7%B4%B9%E4%BB%8B/%E3%81%8A%E5%95%8F%E3%81%84%E5%90%88%E3%82%8F%E3%81%9B%E3%83%95%E3%82%A9%E3%83%BC%E3%83%A0/">
								<p>メールでのお問い合わせはこちら</p>
								<p class="link_name"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/mail.png">お問い合わせフォーム</p>
								<p>24時間受付中</p>
							</a>
							<a href="https://line.me/R/ti/p/%40565ywvmq" target="_blank">
								<p>LINEでのお問い合わせはこちら</p>
								<p class="link_name"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/line.png">LINEでのご相談</p>
								<p>24時間受付中</p>
							</a>
						</div>
					</div>
				</div>
			</section>

			<section class="sec07">
				<div class="w1140">
					<h2>創業融資・公庫融資の代行サポート</h2>

					<ul>
						<li>
							<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img17.png" alt="無料相談"></p>
							<h3>無料相談</h3>
							<p>創業融資の可能性について【無料判定】をさせていただきます。まずはお気軽にご相談ください。</p>
						</li>
						<li>
							<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img18.png" alt="サポート申し込み"></p>
							<h3>サポート申し込み</h3>
							<p>サポート希望される場合は、お申込みをお願いいたします。別途申込書を用意しております。</p>
						</li>
						<li>
							<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img19.png" alt="申請書記入"></p>
							<h3>申請書記入</h3>
							<p>事業計画などの申請書類の作成を、経験豊富な弊社スタッフが親身にサポートいたします。</p>
						</li>
						<li>
							<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img20.png" alt="必要書類準備"></p>
							<h3>必要書類準備</h3>
							<p>書類の記入と平行して、必要書類を準備していただきます。</p>
						</li>
						<li>
							<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img21.png" alt="面談練習"></p>
							<h3>面談練習</h3>
							<p>申請書類の最終チェックや、融資の重要なポイントである面談の練習などを行います。</p>
						</li>
						<li>
							<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img22.png" alt="提出"></p>
							<h3>提出</h3>
							<p>当社にて書類の提出を行います。</p>
						</li>
					</ul>

					<div class="flow">
						<h3>公庫の創業融資の<br class="sp_only">申請の流れ</h3>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img23.png" alt="公庫の創業融資の申請の流れ" class="pc_only">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_img23_sp.png" alt="公庫の創業融資の申請の流れ" class="sp_only">
					</div>
				</div>
			</section>

			<section class="sec08">
				<div class="w1140">
					<h2>創業融資・公庫融資に関するよくあるご質問</h2>

					<dl>
						<dt><span>Q</span>公庫の創業融資のため創業計画書（事業計画書）を作りたいですが、全く作り方がわかりません。このような状況でも融資は可能でしょうか？</dt>
						<dd>もちろん大丈夫です。創業計画書の作成からサポートさせていただきます。創業計画書・事業計画書が完成しましたら、面談練習も実施いたします。書類だけでなく、面接対策もしっかり行わないと創業融資を審査を突破できません。まずは公庫の創業融資に強い、実績のある専門家にご相談ください。もちろん、公庫の創業融資（新創業融資制度等）だけでなく、制度融資・保証協会付き融資もダブルで受けることもできます。お客様に最適なプランを設計させていただいております。お気軽にお問い合せください。</dd>
							
						<dt><span>Q</span>そこまで多額の資金は必要ありません。それでも、多少は多めに借りておいた方が良いのでしょうか？</dt>
						<dd>余裕のある経営を行うためにも、創業時にしっかりと事業資金を確保しておきましょう。多くのお客様が、当初の事業計画より遅れた成長をたどります。昨今の新型コロナによる影響など、見通しのしづらい事業環境が当面は続くと思われます。そのような状況の中で、お客様の命綱となるものが事業資金です。ぎりぎりの資金だけしか準備せずに創業した場合、少しでも計画に遅れが生じると資金がすぐにショートします。最悪の場合、倒産の可能性もあります。「余裕のある事業資金」は創業における命綱といえます。</dd>
						
						<dt><span>Q</span>自己資金が全くありませんが、創業融資は可能でしょうか？</dt>
						<dd>自己資金が全くない・少ない場合には、公庫の審査上、不利に働くことは否めません。お客様の状況に応じた対策を一緒に考えさせていただきたいと思います。まずはご相談ください。</dd>
									
						<dt><span>Q</span>すでに借入やローン（自動車ローン、カードローン、消費者ローン等）がありますが、それでも創業融資はできますか？</dt>
						<dd>借入やローン残高がある場合でも、お客様の状況次第では創業融資の可能性があります。まずはお気軽にご相談ください。</dd>
									
						<dt><span>Q</span>過去にカードの延滞があり、信用情報に少し不安があります</dt>
						<dd>お客様の信用情報（CIC）の確認も一緒に行わせていただきます。必要な対策をとることができれば創業融資を受けることができます。</dd>
									
						<dt><span>Q</span>公庫と保証協会の両方から創業融資を受けたいです。</dt>
						<dd>両方からお借入れすることもできます。必要な書類、申込みのタイミング、順序やスケジュールなど、ノウハウが必要になりますので、まずは弊社にお気軽にご相談ください。</dd>
									
						<dt><span>Q</span>設立して間もない会社ですが、創業融資は可能でしょうか？</dt>
						<dd>問題なく創業融資を受けられます。今後の事業計画をしっかりと策定できれば、必要額をお借入れ可能です。</dd>
									
						<dt><span>Q</span>赤字でも借りられるか？</dt>
						<dd>可能です。公庫や保証協会への最適な説明方法を一緒に考えさせていただきます。</dd>
									
						<dt><span>Q</span>法人の個人事業主のどちらで創業融資を受けるべきでしょうか？</dt>
						<dd>どちらでも融資の可能性は同じになりますので、お選びいただいて問題はありません。お客様のニーズをお伺いし、最適なお申込みの形をアドバイスさせていただきます。</dd>
									
						<dt><span>Q</span>無担保・無保証で創業融資を受けられるというのは本当でしょうか？</dt>
						<dd>はい、公庫の「新創業融資制度」を利用すれば、最大で3000万円までを無担保・無保証でお借入れ可能です。ただし、実際には1000万円が１つのラインとなることが多いです。足りない分は制度融資などをダブルで受けられれば対応可能です。</dd>
								
						<dt><span>Q</span>創業融資のサポートを受けるに際して、税理士の顧問契約は必須でしょうか？</dt>
						<dd>顧問契約は必須ではありません。ただし、弊社は会計事務所のため、確定申告をはじめ、法人設立・経理・税務のトータルサポートを行っております。お気軽にご相談ください。</dd>
							
						<dt><span>Q</span>フランチャイズ起業で公庫の創業融資を利用することは可能でしょうか？</dt>
						<dd>フランチャイズでも創業融資は対象になります。ただし、これまでのご経験と異なる事業で創業されることになることが多いため、オーナー様のこれまでの実務経験・職務経歴を公庫にどのようにアピールするか、重要となります。弊社ではフランチャイズ(FC)を利用した創業融資のサポート経験も豊富です。お気軽にご相談ください。</dd>
									
						<dt><span>Q</span>公庫の創業融資の返済期間は？</dt>
						<dd>概ね、運転資金は7年程度、設備資金は10年程度が多いです。公庫の公式サイトでシュミレーションもできます。ただし、お客様の状況次第で変わりますので、公庫の創業融資サポート、代行の経験豊富な弊社にお気軽にご相談ください。</dd>
					</dl>
				</div>
			</section>

			<section class="sec09">
				<div class="w1140">
					<h2>創業融資・公庫融資に関する情報を随時発信</h2>
				</div>
			</section>
		</div>

	</div>	

		<div style="overflow: hidden; max-width: 1200px; margin: 0 auto;">
			<?php if( is_toppage_style() == "one_column" ) : ?>
				<?php if( ! is_mobile()) :?>
				<main id="main-contents-one" class="main-contents <?php is_animation_style(); ?>" itemscope itemtype="https://schema.org/Blog">
						
					<?php if ( wp_isset_widets( 'home-top-widget',true ) ) : ?>
					<div id="home-top-widget">
					<?php dynamic_sidebar( 'home-top-widget' ); ?>
					</div>
					<?php endif; ?>

					<?php get_template_part('include/liststyle/post-list-mag'); ?>

					<?php if ( wp_isset_widets( 'home-bottom-widget',true ) ) : ?>
					<div id="home-bottom-widget">
					<?php dynamic_sidebar( 'home-bottom-widget' ); ?>
					</div>
					<?php endif; ?>
					
				</main>
			
				<?php else: ?>
			
				<main id="main-contents" class="main-contents <?php is_animation_style(); ?>" itemscope itemtype="https://schema.org/Blog">
					
					<?php if ( wp_isset_widets( 'home-top-widget',true ) ) : ?>
					<div id="home-top-widget">
					<?php dynamic_sidebar( 'home-top-widget' ); ?>
					</div>
					<?php endif; ?>

					<?php if( is_post_list_style() == "magazinestyle" ) : ?>
						<?php get_template_part('include/liststyle/post-list-mag'); ?>
					<?php elseif( is_post_list_style() == "magazinestyle-sp1col" ) : ?>
						<?php get_template_part('include/liststyle/post-list-mag-sp1col'); ?>
					<?php elseif( is_post_list_style() == "basicstyle" ) : ?>
						<?php get_template_part('include/liststyle/post-list'); ?>
					<?php endif; ?>
					
					<?php if ( wp_isset_widets( 'home-bottom-widget',true ) ) : ?>
					<div id="home-bottom-widget">
					<?php dynamic_sidebar( 'home-bottom-widget' ); ?>
					</div>
					<?php endif; ?>

				</main>
				<?php get_sidebar(); ?>
			
				<?php endif; ?>

			<?php elseif( is_toppage_style() == "two_column" ) : ?>
			
				<main id="main-contents" class="main-contents <?php is_animation_style(); ?>" itemscope itemtype="https://schema.org/Blog">
					
					<?php if ( wp_isset_widets( 'home-top-widget',true ) ) : ?>
					<div id="home-top-widget">
					<?php dynamic_sidebar( 'home-top-widget' ); ?>
					</div>
					<?php endif; ?>

					<?php if( is_post_list_style() == "magazinestyle" ) : ?>
						<?php get_template_part('include/liststyle/post-list-mag'); ?>
					<?php elseif( is_post_list_style() == "magazinestyle-sp1col" ) : ?>
						<?php get_template_part('include/liststyle/post-list-mag-sp1col'); ?>
					<?php elseif( is_post_list_style() == "basicstyle" ) : ?>
						<?php get_template_part('include/liststyle/post-list'); ?>
					<?php endif; ?>
					
					<?php if ( wp_isset_widets( 'home-bottom-widget',true ) ) : ?>
					<div id="home-bottom-widget">
					<?php dynamic_sidebar( 'home-bottom-widget' ); ?>
					</div>
					<?php endif; ?>

				</main>
				<?php get_sidebar(); ?>
				
			<?php endif; ?>
		</div>
		
	</div>
	
<?php get_footer(); ?>

<!--アコーディオンjs-->
<script>
$(function() {
	//デフォルト閉じる
	$('.top_sec dt').addClass('active');
	$('.top_sec dt').next('dd').css('display','none');
	
	$('.top_sec dt').on('click',function(){
		$(this).next('dd').slideToggle();
		$(this).toggleClass('active');
	});
});
</script>